To run a simple http server to avoid cross site scripting warnings while developing, run this in the directory of the file you want to serve:

`python -m SimpleHTTPServer 8000`