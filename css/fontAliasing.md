Webkit default:
`-webkit-font-smoothing: subpixel-antialiased;`

Smoother fonts
`-webkit-font-smoothing: antialiased;`

use it like:
```
html{
    -webkit-font-smoothing: antialiased;
}
```