Change the "USERNAME" in the URL to yours, and the "OTHERREPOSITORY" to the name of your new repo

`git remote set-url https://github.com/USERNAME/OTHERREPOSITORY.git`