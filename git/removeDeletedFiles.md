Use this command to remove files that were deleted from the git staging area

`git add -u`